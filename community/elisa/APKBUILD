# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=elisa
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by vlc
arch="all !armhf !s390x"
url="https://kde.org/applications/multimedia/org.kde.elisa"
pkgdesc="A simple music player aiming to provide a nice experience for its users"
license="LGPL-3.0-or-later"
depends="
	kirigami
	kirigami-addons
	qqc2-desktop-style
	qt6-qtbase-sqlite
	vlc
	"
makedepends="
	baloo-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	kirigami-dev
	kirigami-addons-dev
	kpackage-dev
	kxmlgui-dev
	qqc2-desktop-style-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg-dev
	samurai
	vlc-dev
	"
checkdepends="
	cmd:dbus-run-session
	xvfb-run
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/elisa.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/elisa-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	local skipped_tests="("
	local tests="
		viewManagerTest
		mediaplaylistproxymodelTest
		elisaqmltests
		localfilelistingtest
		trackmetadatamodeltest
		"
	case "$CARCH" in
		aarch64) tests="$tests
			databaseInterfaceTest
			" ;;
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	dbus-run-session xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a4ce41dcf811e8fbcd930308b3307f3d2531d705304961008d8123ad24fda49607604e472a2b031b66b0ccf3c101433542389684ba4292b7089cd799c10ce11f  elisa-24.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontact
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# armv7, ppc64le, s390x and riscv64 blocked by qt6-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
# loongarch64 blocked by pimcommon
arch="all !armv7 !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://kontact.kde.org/"
pkgdesc="Container application to unify several major PIM applications within one application"
license="GPL-2.0-or-later"
makedepends="
	akonadi-dev
	extra-cmake-modules
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kontactinterface-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt6-qtbase-dev
	qt6-qtwebengine-dev
	samurai
	"
subpackages=" $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kontact.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontact-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9fea8774a15295842f5144bbcd572559dc74958310e255d5776ea5596a03198b7131c53e359aafe5a2713ca55f3684e17a9ebd4f5f5e9c025d285bf50cc52f38  kontact-24.08.0.tar.xz
"

# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/gnome <ncopa@alpinelinux.org>
pkgname=gnome-online-accounts
pkgver=3.50.4
pkgrel=0
pkgdesc="Single sign-on framework for GNOME"
url="https://wiki.gnome.org/Projects/GnomeOnlineAccounts"
arch="all"
license="GPL-2.0-or-later"
depends="dbus:org.freedesktop.Secrets"
makedepends="
	docbook-xml
	docbook-xsl
	gcr4-dev
	gnome-desktop-dev
	gobject-introspection-dev
	gtk-doc
	itstool
	json-glib-dev
	krb5-dev
	libadwaita-dev
	libsecret-dev
	libxslt
	libxml2-utils
	meson
	rest1-dev
	vala
	webkit2gtk-4.1-dev
	"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/gnome-online-accounts/${pkgver%.*}/gnome-online-accounts-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

dev() {
	provides="$pkgname-static=$pkgver-r$pkgrel"
	replaces="$pkgname-static"
	default_dev
}

sha512sums="
f0f3d6186cd436cac3f6f3624062fedce7035726cf148a75c1929d9e2b3b6813eab774c12fb6d54fb86fcb2154e016abe5a861d847048cd2a38d3b67e8867857  gnome-online-accounts-3.50.4.tar.xz
"

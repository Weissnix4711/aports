# Contributor: solidnerd <niclas@mietz.io>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=glab
pkgver=1.46.0
pkgrel=0
pkgdesc="Open source GitLab CLI tool written in Go"
url="https://gitlab.com/gitlab-org/cli"
arch="all"
license="MIT"
depends="git"
makedepends="go"
options="!check"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/gitlab-org/cli/-/archive/v$pkgver/cli-v$pkgver.tar.gz
	unbump_go.patch
	"
builddir="$srcdir/cli-v$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	# date seems a little broken to override
	go build -ldflags "
		-X main.debugMode=false
		-X main.version=v$pkgver
		-X main.buildDate=$(date -u "+%Y-%m-%d" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
		-extldflags \"$LDFLAGS\"
		" \
		-o bin/glab \
		./cmd/glab/main.go

	# XXX: When glab is run in fakeroot it segfaults for some reason
	# on ppc64le. By generating the compilation files here we
	# workaround that but we need to investigate why it segfaults in
	# fakeroot eventually.
	bin/glab completion --shell bash > bash.comp
	bin/glab completion --shell zsh > zsh.comp
	bin/glab completion --shell fish > fish.comp
}

package() {
	install -Dm755 bin/glab -t "$pkgdir"/usr/bin/

	install -Dm644 bash.comp "$pkgdir"/usr/share/bash-completion/completions/glab.bash
	install -Dm644 zsh.comp "$pkgdir"/usr/share/zsh/site-functions/_glab
	install -Dm644 fish.comp "$pkgdir"/usr/share/fish/vendor_completions.d/glab.fish
}

sha512sums="
64fb4b78f8ed35a9de546fc4b053e2ee2e4de8396a5d3c3fa7477febc429500946a533399e6e8569fe45673c6c977010c785c68cca948baa39e8bb012ebf2d5b  glab-1.46.0.tar.gz
34de704e2ae4f7c2601eb6231a6b6abd9c1dfbcf9ad212f3ac24a6f30d8eef82d76b7660df1a0c541e81de430170a74fa6dd8e501d052e627bfff0b9077528e8  unbump_go.patch
"

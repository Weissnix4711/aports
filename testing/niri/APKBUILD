# Contributor: Ogromny <ogromnycoding@gmail.com>
# Maintainer: Ogromny <ogromnycoding@gmail.com>
pkgname=niri
pkgver=0.1.8
pkgrel=0
pkgdesc="A scrollable-tiling Wayland compositor"
url="https://github.com/YaLTeR/niri"
arch="x86_64 ppc64le aarch64"
license="GPL-3.0-only"
makedepends="
	cargo
	cargo-auditable
	clang-libclang
	clang-libs
	eudev-dev
	glib-dev
	libinput-dev
	libseat-dev
	libxkbcommon-dev
	mesa-dev
	pango-dev
	pipewire-dev
	rust
	"
subpackages="
	$pkgname-portalsconf
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/YaLTeR/niri/archive/refs/tags/v$pkgver.tar.gz"

build() {
	cargo auditable build \
		--release \
		--no-default-features \
		--features dbus,xdp-gnome-screencast
}

check() {
	cargo test \
		--release \
		--no-default-features \
		--features dbus,xdp-gnome-screencast
}

package() {
	install -Dm755 target/release/niri "$pkgdir"/usr/bin/niri
	install -Dm644 resources/niri.desktop "$pkgdir"/usr/share/wayland-sessions
	install -Dm644 "$builddir"/resources/niri-portals.conf -t "$pkgdir"/usr/share/xdg-desktop-portal/
}

portalsconf() {
	pkgdesc="xdg-desktop-portal configuration of compatible portals for Niri"
	install_if="$pkgname=$pkgver-r$pkgrel"
	depends="xdg-desktop-portal xdg-desktop-portal-gnome"
	amove usr/share/xdg-desktop-portal/niri-portals.conf
}


sha512sums="
191e9a781a1d193cc49e3609bfb00a4915e74526ba9d4db8e091e49b515b8b7fc9292706b0cb0bb23321629a92f9ba624a0b13b80827e126ed487ffb780ad963  niri-0.1.8.tar.gz
"
